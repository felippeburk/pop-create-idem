====================
Customize Cloud Spec
====================

.. note::

    Learn more about :doc:`CloudSpec grammar</topics/references/cloud_spec/>`

`pop-create-idem` auto-generates Idem cloud plugin from Swagger or OpenAPI specification. First, API documentation is
converted into CloudSpec. The CloudSpec can be customized before `pop-create-idem` generates `exec`, `states`, and
`tools` modules for the resources. Some use cases of such customizations is to rename resource names, change certain
parameter names, add folder etc.

Pre-Requisites
--------------
* The generated plugin should be installed (via pip) in the working python environment where `pop-create` command will be run.
* Generate an Idem plugin with `pop-create-idem` from Swagger/OpenAPI.
* The customization plugin should be present under generated plugin:

    .. code-block:: bash

        │──{simple_service_name}
        │   ├── cloudspec
        │       ├── contracts
        │       └── customize
        │           └── {simple_service_name}.py

Command
-------

Use `--cloud-spec-customize-plugin` CLI config to provide one ore more customize plugins.

.. code-block:: bash

    pop-create swagger --directory /path/to/new/project --specification={swagger-spec-yaml-or-accessible-swagger-spec-json-url} --project-name=idem-{my_cloud} --simple_service_name={my_cloud} --cloud-spec-customize-plugin={simple_service_name} --author={company_name}

Alternatively, `--customize-plugin` can also be used.

.. code-block:: bash

    pop-create swagger --directory /path/to/new/project --specification={swagger-spec-yaml-or-accessible-swagger-spec-json-url} --project-name=idem-{my_cloud} --simple_service_name={my_cloud} --author={company_name} --customize-plugin={simple_service_name}

In order to run multiple customization plugins, run:

.. code-block:: bash

    pop-create swagger --directory /path/to/new/project --specification={swagger-spec-yaml-or-accessible-swagger-spec-json-url} --project-name=idem-{my_cloud} --simple_service_name={my_cloud} --author={company_name} --cloud-spec-customize-plugin {plugin_1} {plugin_2} {plugin_3}

Example
-------
We will use petstore swagger for generating Idem cloud plugin and customizing CloudSpec to add a sub folder for each resource.

* To generate `idem-petstore` plugin from a sample `petstore <https://petstore.swagger.io/v2/swagger.json>`_ swagger, run:

.. code-block:: bash

    pop-create -n idem-petstore --cloud petstore --specification https://petstore.swagger.io/v2/swagger.json --directory /path/to/new/project --author "Petstore Inc."

* To install `idem-petstore` plugin in the python environment, run:

.. code-block:: bash

    pip install -e {idem-petstore-root-dir}

* Edit `idem-petstore/cloudspec/customize/petstore.py` file and add the following line of code under `run` function:

.. code-block:: python

    updated_cloud_spec: NamespaceDict = copy.deepcopy(ctx.cloud_spec)
    for name, plugin in ctx.cloud_spec.get("plugins").items():
        new_plugin = updated_cloud_spec.get("plugins").pop(name)
        # Will add new folder under modules before their exec/states/tools modules
        new_resource_name = "new." + name
        for func_name, func_data in new_plugin.get("functions").items():
            func_data.get("hardcoded", {})["resource_name"] = new_resource_name
        updated_cloud_spec["plugins"][new_resource_name] = new_plugin

    ctx.cloud_spec = updated_cloud_spec

* To re-generate new `idem-petstore` plugin from same swagger, run:

.. code-block:: bash

    pop-create -n idem-petstore --cloud petstore --specification https://petstore.swagger.io/v2/swagger.json --directory /new/path/to/new/project --cloud-spec-customize-plugin=petstore --author="Petstore Inc."

You should see the following directory structure:

.. code-block:: bash

    .
    ├── CONTRIBUTING.rst
    ├── LICENSE
    ├── README.rst
    ├── __pycache__
    │   ├── noxfile.cpython-311.pyc
    │   └── setup.cpython-311.pyc
    ├── build.conf
    ├── docs
    │   ├── conf.py
    │   ├── index.rst
    │   ├── releases
    │   │   ├── 0.1.0.rst
    │   │   └── index.rst
    │   ├── sitevars.rst
    │   ├── topics
    │   │   ├── contributing.rst
    │   │   └── license.rst
    │   └── tutorial
    │       ├── example.rst
    │       └── index.rst
    ├── example
    │   ├── absent
    │   │   ├── init.sls
    │   │   ├── new.pet.sls
    │   │   ├── new.store.sls
    │   │   └── new.user.sls
    │   └── present
    │       ├── init.sls
    │       ├── new.pet.sls
    │       ├── new.store.sls
    │       └── new.user.sls
    ├── idem_petstore
    │   ├── acct
    │   │   ├── contracts
    │   │   └── petstore
    │   │       ├── basic_auth.py
    │   │       └── default_auth.py
    │   ├── autogen
    │   │   └── petstore
    │   │       └── templates
    │   │           ├── exec
    │   │           │   ├── create.jinja2
    │   │           │   ├── delete.jinja2
    │   │           │   ├── get.jinja2
    │   │           │   ├── list.jinja2
    │   │           │   └── update.jinja2
    │   │           ├── sample.jinja2
    │   │           ├── state
    │   │           │   ├── absent.jinja2
    │   │           │   ├── describe.jinja2
    │   │           │   └── present.jinja2
    │   │           ├── tests
    │   │           │   ├── exec
    │   │           │   │   ├── test_create.jinja2
    │   │           │   │   ├── test_delete.jinja2
    │   │           │   │   ├── test_get.jinja2
    │   │           │   │   ├── test_list.jinja2
    │   │           │   │   └── test_update.jinja2
    │   │           │   ├── states
    │   │           │   │   ├── test_absent.jinja2
    │   │           │   │   ├── test_describe.jinja2
    │   │           │   │   └── test_present.jinja2
    │   │           │   └── tool
    │   │           │       └── default.jinja2
    │   │           └── tool
    │   │               └── default.jinja2
    │   ├── cloudspec
    │   │   ├── contracts
    │   │   └── customize
    │   │       └── petstore.py
    │   ├── conf.py
    │   ├── exec
    │   │   ├── contracts
    │   │   └── petstore
    │   │       ├── init.py
    │   │       ├── new
    │   │       │   ├── pet.py
    │   │       │   ├── store.py
    │   │       │   └── user.py
    │   │       ├── sample.py
    │   │       └── v1_0_6
    │   │           ├── init.py
    │   │           └── recursive_contracts
    │   │               └── init.py
    │   ├── states
    │   │   ├── contracts
    │   │   └── petstore
    │   │       ├── init.py
    │   │       ├── new
    │   │       │   ├── pet.py
    │   │       │   ├── store.py
    │   │       │   └── user.py
    │   │       └── sample.py
    │   ├── tool
    │   │   ├── contracts
    │   │   └── petstore
    │   │       ├── new
    │   │       │   ├── pet.py
    │   │       │   ├── store.py
    │   │       │   └── user.py
    │   │       └── test_state_utils.py
    │   └── version.py
    ├── noxfile.py
    ├── requirements
    │   ├── base.txt
    │   ├── docs.txt
    │   ├── py3.10
    │   │   └── tests.txt
    │   ├── py3.11
    │   │   └── tests.txt
    │   ├── py3.8
    │   │   └── tests.txt
    │   ├── py3.9
    │   │   └── tests.txt
    │   └── tests.in
    ├── setup.cfg
    ├── setup.py
    └── tests
        ├── integration
        │   ├── conftest.py
        │   ├── exec
        │   │   └── new
        │   │       ├── test_pet.py
        │   │       ├── test_store.py
        │   │       └── test_user.py
        │   ├── states
        │   │   └── new
        │   │       ├── test_pet.py
        │   │       ├── test_store.py
        │   │       └── test_user.py
        │   └── tool
        │       └── new
        │           ├── test_pet.py
        │           ├── test_store.py
        │           └── test_user.py
        └── unit
            └── conftest.py
