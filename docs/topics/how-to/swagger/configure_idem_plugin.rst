========================
Configure an Idem plugin
========================

After you generate an Idem Cloud plugin from a Swagger or OpenAPI specification, you must configure the plugin so you can run your first Idem command on the resources.

#. Update the acct plugin
#. Update the cloud resource's exec functions
#. Update the cloud resource's state functions

.. note::

    Look for `TODO` placeholders in each module. You need to update the code as directed with respect to each cloud provider.

Acct plugin
+++++++++++
There are three auth plugins created by default as part of `pop-create`:

* `basic_auth`: Handles authentication with the provider with `username/password`.
* `api_key_auth`: Handles authentication with the provider with an `api_key`.
* `client_credentials_auth`: Handles authentication with the provider with `client-id/secret/tenant`.

.. note::

   You can write your own acct plugin if the default plugin does not satisfy the need of the cloud provider.
    You can keep or remove the default acct plugin.

Exec module
+++++++++++

`pop-create` generates get, list, create, update, and delete functions for each identified resource in the Swagger/OpenAPI specification:

In the exec module, you need to update:

* The mapping of the resource's unique identifier to the `resource_id` field in the function definition headers
* The mapping of the resource's unique identifier to the `resource_id` in the returned response (get/list/create/update functions)
* The mapping of the response object into the result

Special handling may be required for the `update` function, such as the parameters to update.

States module
+++++++++++++

`pop-create` generates present, absent, and describe functions for each identified resource in the Swagger/OpenAPI specification. These functions use respective exec functions as needed.

In the states module, you need to update:

* The mapping of the resource's unique identifier to the `resource_id` field in the function definition headers
* The `present` function, if needed for computing resource changes


Next steps
++++++++++

* Learn more about :doc:`customizing Cloud Spec<customize_cloud_spec>`
* Learn more about `getting started with Idem <https://docs.idemproject.io/getting-started>`_.
