==========
Cloud Spec
==========

`pop-create-idem` uses a CloudSpec dictionary to generate Idem's exec, states, and tool modules.
The plugins for `pop-create-idem` transforms API documentation into a CloudSpec.

CloudSpec Grammar
+++++++++++++++++

CloudSpec
---------

.. code-block:: python

    CloudSpec = {
        project_name: str,
        service_name: str,
        api_version: str,
        request_format: dict[str, str],
        plugins: dict[str, CloudSpecPlugin],
    }

* project_name (str): Conventionally called "idem_<my_project>".
* service_name (str): Typically just "<my_project>".
* api_version (str): Used for differentiating between versions of the API you're consuming. Optional, default: 'latest'.
* request_format  (Dict[str, str], Optional): A mapping of function names to a cookiecutter template string that describes how the function should be called.
* plugins (Dict[str, CloudSpecPlugin], Optional): A mapping of a plugin ref's to their CloudSpecPlugin definition.

CloudSpecPlugin
---------------

.. code-block:: python

    CloudSpecPlugin = {
        doc: str,
        imports: list[str],
        virtual_imports: list[str],
        contracts: list[str],
        func_alias: dict[str, str],
        virtualname: str,
        sub_virtual: bool,
        sub_alias: list[str],
        functions: dict[str, CloudSpecFunction],
        file_vars: dict[str, any],
    }

* doc (str): Docstring for the function.
* imports (List[str], Optional): List of Python imports required for this plugin.
* virtual_imports (List[str], Optional): List of virtual imports. The plugin will not load onto the hub if these imports fail.
* contracts (List[str], Optional): List of contracts enforced by this plugin.
* func_alias (Dict[str, str], Optional): Dictionary mapping function names that clash with Python keywords to their intended names on the hub.
* virtualname (str, Optional): Virtual name for this plugin on the hub if its name clashes with a Python keyword or a standard Python library.
* sub_virtual (bool, Optional): For "init.py" files specifically. A condition that would prevent all files in the same directory from being loaded onto the hub.
* sub_alias (List[str], Optional): Also for "init.py" files. An alias for its parent directory, to be used if the parent directory clashes with a Python keyword or standard library.
* functions (Dict[str, CloudSpecFunction], Optional): Mapping of function names to a CloudSpecFunction.
* file_vars (Dict[str, Any], Optional): Any other arbitrary key/values to pass to the plugin spec.

CloudSpecFunction
-----------------

.. code-block:: python

    CloudSpecFunction = {
        doc: str,
        return_type: str,
        hardcoded: dict[str, str],
        params: dict[str, CloudSpecParam],
    }

* doc (str): Description of the function.
* return_type (Any, Optional): Type hint for the return value of the function.
* hardcoded (Dict[str, str], Optional): Mapping of keys to strict values that should be used as-is.
* params(Dict[str, CloudSpecParam], Optional): Mapping of parameter names to the CloudSpecParam object.

CloudSpecParam
--------------

.. code-block:: python

    CloudSpecParam = {
        name: str,
        required: bool,
        target: str,
        target_type: str,
        member: CloudSpecMember,
        param_type: str,
        doc: str,
        default: any,
        snaked: str,
    }

* name (str): Name of the parameter.
* required (bool): True if the parameter is required, else False.
* target (str): Indicates how this parameter is consumed in the function (i.e., is it part of "kwargs", or does it have an alias?).
* target_type(str): Indicates how the parameter should be consumed by create plugins (i.e., "mapping", "arg").
* member (CloudSpecMember, Optional): For dictionary type parameters that need to create a dataclass.
* param_type (str, Optional): Type hint for the parameter.
* doc (str, Optional): Docstring for the parameter.
* default (Any, Optional): Default value for the parameter.
* snaked (str, Optional): The `snake_cased` version of the parameter. (Evaluated if not provided)


CloudSpecMember
---------------

.. code-block:: python

    CloudSpecMember = {name: str, params: dict[str, CloudSpecParam]}

* name (str): Name of a dataclass from a param.
* params (Dict[str, CloudSpecParam]): Params object to be used within the new data class.
